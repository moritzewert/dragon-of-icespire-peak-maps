# Dragon of Icespire Peak Maps

This FoundryVTT module is compilation of maps for the Dragon of Icespire Peak adventure. It includes all maps for the main chapters.

All maps are designed for for [FoundryVTT](https://foundryvtt.com/) with walls, doors, lighting and windows set up. They do not have exciting effects like dynamic roofs or teleports between levels or similar. My knowledge of Foundry is limited so any errors or improvements please let me know. 

I make maps purely as a hobby so all of the maps are offered for free, but if you have money burning a hole in your pocket, these maps would not have been possible with the wonderful assets, in particular, the 2 Minute Tabletop ones, and the Dungeondraft software, so consider a Patreon subscription or purchase there.

# Credit

[2-Minute Tabletop](https://2minutetabletop.com/) - Ross's assets and maps are just wonderful and are the primary source of art for these maps.

[Crosshead Studios](https://crossheadstudios.com/) - while I don't use his work assets extensively (although it is great overall), I do use the Crosshead textures for grass and rock terrain.

[Dungeon Mapster](https://www.patreon.com/dungeonmapster/) - big fan of Ryan's work and it aligns very well with the 2MTT style. I used a scattering of assets from him.

Gogots - I used a scattering of assets made by gogots to fill in where there were no 2MTT ones available.

Other utility assets - additional lighting, shadows and clouds/dust effects came from Apprentice of Aule, Gnome Factory, Krager and Crave. Their work can be found on [Cartography Assets](https://cartographyassets.com/asset-category/specific-assets/dungeondraft/)

# More Maps

I make maps for the games I run and simply just for the fun of it. They are freely available to download [here on Google Drive](https://drive.google.com/drive/folders/1jZMoDOZBe-y5cRIGl8wRQVjeSnisZppN?usp=sharing). You can go ahead and use them without installing this module, and you will find maps assets if you want to try dynamic roofs or similar!

# More Maps

*uchidesi34's Dragon of Icespire Peak Map Pack is unofficial Fan Content permitted under the Fan Content Policy. Not approved/endorsed by Wizards. Portions of the materials used are property of Wizards of the Coast. ©Wizards of the Coast LLC.*

# Installation

Once you have installed and activated the module, the scenes will appear as in a compendium in your game.

```
https://gitlab.com/uchideshi34/dragon-of-icespire-peak-maps/-/raw/main/module.json
```

# Versions

v1.1.2 - Fixed a range of minor issues to do with missing walls, gaps in walls and corridors being too narrow

v1.1.1 - Updated Umbrage Hill (levels) map as it was a non-levels version

v1.1.0 - Added support for Levels across all relevant maps

v1.0.2 - Added missing map for Icespire Hold upper floor

v1.0.1 - Updated Falcon's Hunting Lodge top floor to reference correct background image

v1.0.0 - Initial baseline

[![Preview Image](zip/Preview_Page_DoIP.webp)](https://gitlab.com/uchideshi34/dragon-of-icespire-peak-maps/-/raw/main/zip/Preview_Page_DoIP.webp)




